# singularity exec -B /data/mcdonnellbms/Duke-BMS:/data/mcdonnellbms/Duke-BMS:rw /data/dcibioinformatics/sifs/dcibioinformaticsR-v1.sif R

# ATAC-Seq
library(tidyverse)
library(openxlsx)

wd <- "/data/mcdonnellbms/Duke-BMS"
outdir <- file.path(wd, "Proc/Manifests_to_BMS")

# ------- Levels -------
trts <- levels <- c(
  "Vehicle-FBS", "Enz-FBS", "VHL-FBS", "ARLDD-FBS",
  "Vehicle-CFS-0", "Enz-CFS", "VHL-CFS", "ARLDD-CFS",
  "R1881-CFS-0.01", "R1881-CFS-0.03", "R1881-CFS-0.06", "R1881-CFS-10",
  "R1881-CFS-0.1"
)

# ------- LNCaP (Safi_7736_220506A6) -------

# ------- Manifest: Duke_ATAC-Seq_Manifest_LNCaP.txt -------
md5file <- file.path(wd, "RawData/Safi_7736_220506A6/Safi_7736_220506A6.checksum")
md5 <- read.table(md5file, header = F, col.names = c("FILENAME", "checksum")) %>%
  transmute(
    SAMPLE = gsub("^(\\d+)-.*$", "7736-L\\1", FILENAME),
    FILENAME,
    ASSAYMETHOD = "ATAC-Seq",
    QCFLAG = 0,
    NOTE = "",
    checksum
  ) %>%
  arrange(SAMPLE)
head(md5)

write.table(md5, file = file.path(outdir, "Duke_ATAC-Seq_Manifest_LNCaP.txt"),
            sep = "\t", row.names = F, quote = F)


# ------- Metadata: Duke_ATAC-Seq_Metadata_LNCaP.xlsx -------

ss <- read.xlsx(file.path(wd, "RawData/Manifests/LNCaP ATACseq_order_7736_samples.xlsx"), 
                sheet = 1, startRow = 24) %>%
  separate(Label, sep = " ", remove = F,
           into = c("idx", "tmp", "media", "drug", "dose_w_units")) %>%
  separate(tmp, sep = "-", remove = T,
           into = c("biosample_term", "biological_replicate_number")) %>%
  transmute(
    source_id = Code,
    biosample_term,
    biological_replicate_number,
    media, 
    drug = drug %>% gsub("-", "", .) %>% gsub("Veh", "Vehicle", .),
    compound = paste0(drug, "_", media) %>% gsub("R1881_CFS", "R1881", .),
    dose = gsub("(.*)(nM|uM)$", "\\1", dose_w_units),
    dose_units = dose_w_units %>% gsub(".*(nM|uM)$", "\\1", .) %>% gsub("um", "µM", .),
    dose_w_units,
    condition = paste0(drug, "-", media, 
                       ifelse(grepl("R1881", drug), paste0("-", dose), "")) %>%
                  gsub("Vehicle-CFS", "Vehicle-CFS-0", .),
    celgene_id = as.integer(factor(condition, levels = trts)),
    short_id = gsub("^\\d+-(.*)$", "\\1", source_id),
    short_tm = substr(biosample_term, 1, 1),
    short_ds = ifelse(grepl("R1881", compound), gsub("0\\.", "p", dose), NA)
  ) %>%
  unite(col = "display_name", na.rm = T, remove = F, sep = "_", 
        source_id, biosample_term, compound, dose_w_units, biological_replicate_number) %>%
  unite(col = "display_name_short", na.rm = T, remove = F, sep = "_",
        short_id, short_tm, compound, short_ds)

# table(ss$condition , ss$celgene_id)
# head(ss)
# dim(ss)
# dim(md5)
# table(ss$media)
# table(ss$drug)
# table(ss$compound)

meta <- md5 %>%
  transmute(
    source_id = SAMPLE,
    FILENAME,
    lane = gsub(".*_L00(\\d)_.*", "\\1", FILENAME),
    read = gsub(".*_R(\\d)_.*", "\\1", FILENAME)
  ) %>%
  pivot_wider(
    id_cols = "source_id",
    names_from = "read",
    names_glue = "read{read}_filename",
    values_from = FILENAME,
    values_fn = function(fs){
      gsub("_L00\\d_", "_L00*_", fs) %>%
        unique()
    }
  ) %>%
  left_join(ss, ., by = "source_id") %>%
  mutate(source = "GCB Sequencing and Genomic Technologies Shared Resource",
         source_project_name = "Safi_7736_220506A6",
         DA_project_id = "DA0001216", 
         celgene_project_desc = "CC-94676", 
         assay_type = "ATAC-Seq",
         experiment = "Duke_ATAC",
         biosample_type = "cell line", 
         organism = "Homo sapiens",
         technical_replicate_number = 1,
         antibody_target = "",
         is_normal = "yes", # 
         introduced_construct = "None",
         treatment_duration = "24", 
         treatment_duration_units = "hours",
         prep_method = "Nextera DNA Library Prep Kit", #
         paired_end = "paired-end", 
         stranded = "NONE", #
         spike_ins = "NONE", 
         platform = "Illumina NovaSeq 6000", #
         merged_groups = "", 
         UMI = "", 
         UMI_filename = "", 
         other_data_filename = "") %>%
  # arrange(as.numeric(idx)) %>%
  dplyr::select(source, source_project_name, source_id, 
                DA_project_id, celgene_project_desc, assay_type,
                organism, biosample_type, biosample_term,
                display_name, display_name_short, celgene_id,
                experiment, biological_replicate_number,
                technical_replicate_number, antibody_target, 
                is_normal, introduced_construct, 
                compound, dose, dose_units, 
                treatment_duration, treatment_duration_units,
                prep_method, paired_end, stranded, spike_ins, 
                platform, read1_filename, read2_filename, 
                merged_groups, UMI, UMI_filename, other_data_filename)

write.xlsx(meta, file = file.path(outdir, "Duke_ATAC-Seq_Metadata_LNCaP.xlsx"), 
           overwrite = T)
# Qs: is_normal? prep_method for vcap, celgene_id order, displaynameshort for rna 2 celllines differ, unique?

# o <- read.xlsx(file.path(outdir, "old/Duke_ATAC-Seq_Metadata_LNCaP.xlsx"))
# n= meta
# 
# dim(o)
# dim(n)
# identical(colnames(o), colnames(n))
# for(nm in colnames(o)){
#   if(!identical(o[[nm]], n[[nm]])){
#     print(nm)
#     print(which(o[[nm]] != n[[nm]]))
#     print("o:")
#     print(o[[nm]])
#     print("n:")
#     print(n[[nm]])
#     print("-------")
#   }
# }


# ------- VCaP -------
# ------- Manifest -------
md5file <- file.path(wd, "RawData/Safi_6886_210423B7/Safi_6886_210423B7.checksum")
md5 <- read.table(md5file, header = F, col.names = c("checksum", "FILENAME")) %>%
  transmute(
    SAMPLE = gsub(".*/(\\d+-[^-]+)-.*$", "\\1", FILENAME),
    FILENAME = basename(FILENAME),
    ASSAYMETHOD = ifelse(grepl("^6886-L", FILENAME), "ATAC-Seq", "RNA-Seq"),
    QCFLAG = 0,
    NOTE = "",
    checksum
  ) %>%
  arrange(SAMPLE)
head(md5)

md5 %>%
  filter(ASSAYMETHOD == "RNA-Seq") %>%
  write.table(file = file.path(outdir, "Duke_RNA-Seq_Manifest_VCaP.txt"),
              sep = "\t", row.names = F, quote = F)
md5 %>%
  filter(ASSAYMETHOD == "ATAC-Seq") %>%
  write.table(file = file.path(outdir, "Duke_ATAC-Seq_Manifest_VCaP.txt"),
              sep = "\t", row.names = F, quote = F)


# ------- Metadata -------

ss_rna <- file.path(wd, "RawData/Manifests/VCaP_RNAseq_ATACseq_order_6886/Stranded mRNA-Seq Samples.xlsx") %>%
  read.xlsx() %>%
  transmute(Code, Label, 
            assay_type = "RNA-Seq",
            experiment = "Duke_RNA")
ss_atac <- file.path(wd, "RawData/Manifests/VCaP_RNAseq_ATACseq_order_6886/ATAC-Seq Samples.xlsx") %>%
  read.xlsx() %>%
  transmute(Code, Label, 
            assay_type = "ATAC-Seq",
            experiment = "Duke_ATAC")

ss <- rbind(ss_rna, ss_atac) %>%
  separate(Label, sep = " ", remove = F,
           into = c("tmp", "media", "drug", "dose_w_units")) %>%
  separate(tmp, sep = "-", remove = T,
           into = c("biosample_term", "biological_replicate_number")) %>%
  transmute(
    source_id = Code,
    biosample_term,
    biological_replicate_number,
    media, 
    drug = drug %>% gsub("-", "", .) %>% gsub("Veh", "Vehicle", .),
    compound = paste0(drug, "_", media) %>% gsub("R1881_CFS", "R1881", .),
    dose = gsub("(.*)(nM|uM)$", "\\1", dose_w_units),
    dose_units = dose_w_units %>% gsub(".*(nM|uM)$", "\\1", .) %>% gsub("um", "µM", .),
    dose_w_units,
    condition = paste0(drug, "-", media, 
                       ifelse(grepl("R1881", drug), paste0("-", dose), "")) %>%
      gsub("Vehicle-CFS", "Vehicle-CFS-0", .),
    celgene_id = as.integer(factor(condition, levels = trts)),
    short_id = gsub("^\\d+-(.*)$", "\\1", source_id),
    short_tm = substr(biosample_term, 1, 1),
    short_ds = ifelse(grepl("R1881", compound), gsub("0\\.", "p", dose), NA)
  ) %>%
  unite(col = "display_name", na.rm = T, remove = F, sep = "_", 
        source_id, biosample_term, compound, dose_w_units, biological_replicate_number) %>%
  unite(col = "display_name_short", na.rm = T, remove = F, sep = "_",
        short_id, short_tm, compound, short_ds)



ss <- rbind(ss_rna, ss_atac) %>%
  mutate(tmp = Label %>% gsub("- ", "-", .) %>% gsub("AR-LDD", "ARLDD", .)) %>%
  separate(tmp, into = c("idx", "biosample_term", "biological_replicate_number", "celgene_id", "condition"), sep = "-") %>%
  separate(condition, into = c("c1", "c2", "d"), sep = " ", remove = F) %>%
  mutate(compound = paste0(c2, "_", c1),
         dose = gsub("[unM]", "", d),
         dose_units = str_match(d, "\\d([a-zA-Z]+$)$")[, 2]) %>%
  mutate(compound = recode(compound, 
                           Veh_FBS = "Vehicle_FBS",
                           Veh_CFS = "Vehicle_CFS",
                           R1881_CFS = "R1881"),
         dose_units = recode(dose_units, uM = 'µM')) %>%
  # group_by(assay_type, compound, dose, dose_units) %>%
  # mutate(biological_replicate_number2 = 1:n(),
  #        technical_replicate_number = 1) %>%
  # ungroup() %>%
  mutate(tmp2 = paste0(compound, "_", dose, dose_units) %>% 
           gsub("_NANA", "", .), 
         # display_name = paste0(Code, "_VCaP_",
         #                       gsub(" ", "_", condition)),
         display_name = paste0(Code, "_VCaP_", tmp2),
         display_name_short = paste0(gsub("6886-", "", Code), "_", tmp2)
  )


meta <- md5 %>%
  transmute(
    source_id = SAMPLE,
    FILENAME,
    lane = gsub(".*_L00(\\d)_.*", "\\1", FILENAME),
    read = gsub(".*_R(\\d)_.*", "\\1", FILENAME)
  ) %>%
  pivot_wider(
    id_cols = "source_id",
    names_from = "read",
    names_glue = "read{read}_filename",
    values_from = FILENAME,
    values_fn = function(fs){
      gsub("_L00\\d_", "_L00*_", fs) %>%
        unique()
    }
  ) 

f <- file.path(wd, "checksum_dds") %>%
  read.table(header = F, col.names = c("md5", "fastq")) %>%
  transmute(filename = gsub("Safi_6886_210423B7/", "", fastq), #%>% gsub("L00\\d", "L00*", .),
            Code = str_match(filename, "^(6886-[SL]\\d+)-")[, 2],
            read = str_match(filename, "_R(\\d)_")[, 2]) %>%
  pivot_wider(names_from = read,
              names_glue = "read{read}_filename",
              values_from = filename) 


m <- m0 %>%
  mutate(source = "GCB Sequencing and Genomic Technologies Shared Resource",
         source_project_name = "Safi_6886_210423B7",
         source_id = Code,
         DA_project_id = "DA0001216",
         celgene_project_desc = "CC-94676",
         biosample_type = "cell line", 
         organism = "Homo sapiens",
         technical_replicate_number = 1,
         antibody_target = "",
         is_normal = "no",
         introduced_construct = "None",
         treatment_duration = "24", 
         treatment_duration_units = "hours",
         prep_method = "KAPA Stranded mRNA-Seq Kit", 
         paired_end = "paired-end", 
         stranded = "REVERSE", 
         spike_ins = "NONE", 
         platform = "Illumina NovaSeq 6000",
         merged_groups = "", 
         UMI = "", 
         UMI_filename = "", 
         other_data_filename = "") %>%
  arrange(assay_type, as.numeric(idx)) %>%
  dplyr::select(source, source_project_name, source_id, 
                DA_project_id, celgene_project_desc, assay_type,
                organism, biosample_type, biosample_term,
                display_name, display_name_short, celgene_id,
                experiment, biological_replicate_number,
                technical_replicate_number, antibody_target, 
                is_normal, introduced_construct, 
                compound, dose, dose_units, 
                treatment_duration, treatment_duration_units,
                prep_method, paired_end, stranded, spike_ins, 
                platform, read1_filename, read2_filename, 
                merged_groups, UMI, UMI_filename, other_data_filename)

meta %>%
  filter(m, assay_type == "RNA-Seq") %>%
  write.xlsx(file = file.path(outdir, "Duke_RNA-Seq_Metadata_VCaP.xlsx"), overwrite = T)
meta %>%
  filter(m, assay_type == "ATAC-Seq") %>%
  write.xlsx(file = file.path(outdir, "Duke_ATAC-Seq_Metadata_VCaP.xlsx"), overwrite = T)

# ------- Cumulative ATAC-Seq -------
mf_vcap <- read.delim(file.path(outdir, "Duke_ATAC-Seq_Manifest_VCaP.txt"))
mf_lncap <- read.delim(file.path(outdir, "Duke_ATAC-Seq_Manifest_LNCaP.txt"))
mf_cm <- rbind(mf_vcap, mf_lncap)
str(mf_cm)
write.table(mf_cm, sep = "\t", row.names = F, quote = F, 
            file = file.path(outdir, "Duke_ATAC-Seq_Manifest_cumulative.txt"))

mt_vcap <- read.xlsx(file.path(outdir, "Duke_ATAC-Seq_Metadata_VCaP.xlsx"))
mt_lncap <- read.xlsx(file.path(outdir, "Duke_ATAC-Seq_Metadata_LNCaP.xlsx"))
mt_cm <- rbind(mt_vcap, mt_lncap)
write.xlsx(mt_cm, file = file.path(outdir, "Duke_ATAC-Seq_Metadata_cumulative.xlsx"), overwrite = T)
